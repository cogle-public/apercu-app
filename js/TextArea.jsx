import React from "react";
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';

require('../css/questions.css');

var $ = require('jquery');

export default class TextArea extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}};
    }

    keyPress(e) {
        e.preventDefault();
        let answer = e.target.value;
        this.props.question.validValue = answer && answer.length > 0;
        this.props.question.answer = answer;
        if (this.props.onDoneFn) {
            this.props.onDoneFn(e.target.value);
        }
    }

    render () {
        let question = this.props.question;
        let metaData = {label:'', helperText: ''};
        if (question.meta_data) {metaData = JSON.parse(question.meta_data)}

        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>
                <div className='question-body question-width text-area-font'>

                    <TextField
                        multiline={true}
                        rows={metaData.rows || 5}
                        onChange={(e) => {this.keyPress(e)}}
                        placeholder={question.answer || metaData.label}
                        label={metaData.label}
                        variant="outlined"
                        required={question.required}
                    />

                </div>

                {question.required
                    ?
                    <div className='hbox'>
                        {'*required'}
                    </div>
                    :
                    ''
                }
            </div>
        );
    }
}


TextArea.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
