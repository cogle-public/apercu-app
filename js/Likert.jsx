import React from "react";
import PropTypes from 'prop-types'
import _ from 'lodash'
const uuid = require('uuid/v4');

import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';

require('../css/questions.css');

import {MDCFormField} from '@material/form-field';
import {MDCRadio} from '@material/radio';

var $ = require('jquery');

export default class Likert extends React.Component {
    constructor() {
        super();
        this.state = {lastAnswer: ''};
        this.uuid = uuid();
        this.choices = ['Strongly Disagree', 'Disagree', 'Agree', 'Strongly agree'];
        this.extraChoice = "Dont't know / NA.";
        this.ec1St = Math.random(0.5) > 0.5;
    }

    setSelection(e, answer) {
        e.preventDefault();
        let myThis = this;
        this.props.question.validValue = true
        this.props.question.answer = answer
        if (this.props.onDoneFn) {this.props.onDoneFn(answer);}
        setTimeout(() => {myThis.setState({lastAnswer: answer})}, 200);
    }

    addExtraChoice(insNow) {
        if (insNow) {
            return (
                            <FormControlLabel
                                key={'mc_ec'}
                                className="likert-radio"
                                control={
                                    <Radio 
                                        checked={this.state.lastAnswer == this.extraChoice} 
                                        onChange={(e) => {this.setSelection(e, this.extraChoice, false)} }
                                        color="primary"
                                        disableRipple={false}
                                    />
                                }
                                label={this.extraChoice}
                                labelPlacement="bottom"
                            />
                )
        }
        return (<span></span>)
    }

    addExtraSpace(insNow) {
        if (insNow) {
            return (
                <span className='linkert-space' >
                </span>
            )
        }
        return (<span></span>)
    }

    render () {
        let question = this.props.question;
        let myThis = this;
        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>

                <div className='question-body question-width'>
                    <FormLabel component="legend">
                        {'pick one' + (question.required ? '*' : '')}
                    </FormLabel>

                    <div className="question-hbox likert-hbox" >
                    {this.addExtraChoice(this.ec1St)}
                    {this.addExtraSpace(this.ec1St)}
                    {_.map(this.choices, (choice, inx) => {
                        return (
                            <FormControlLabel
                                key={'mc_' + inx + '_' + myThis.uuid}
                                className="likert-radio"
                                control={
                                    <Radio 
                                        checked={myThis.state.lastAnswer == choice} 
                                        onChange={(e) => {myThis.setSelection(e, choice, false)} }
                                        color="primary"
                                        disableRipple={false}
                                    />
                                }
                                label={choice}
                                labelPlacement="bottom"
                            />
                        );
                    })
                    }
                    {this.addExtraSpace(!this.ec1St)}
                    {this.addExtraChoice(!this.ec1St)}
                    </div>
                </div>

                {question.required
                        ?   
                        <div className='hbox'>
                            <br/>
                            {'*required'}
                        </div>
                        :
                        ''
                }
            </div>
        );
    }
}


Likert.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
