import React from "react";
import PropTypes from 'prop-types'
import Parser from 'html-react-parser';
import PGHelpers from "./PGHelpers";

export default class FileHtml extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}, copied: false, html: ''};

    }

    componentDidMount() {
        let myThis = this;
        let metaData = JSON.parse(this.props.question.meta_data);
        this.props.question.validValue = true;
        this.props.question.answer = 'N/A';
        if (this.props.onDoneFn) {this.props.onDoneFn();}

        PGHelpers.fetchStaticFile(this.props.turkId, this.props.studyId, metaData.study_dir, metaData.fileName, (r) => {
            //console.log("Set new state: " + r);
            myThis.setState({html: r});
        });
    }

    render () {
        let question = this.props.question;

        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>
                <div className='question-body'>
                    {Parser(this.state.html)}
                </div>
            </div>
        );
    }
}


FileHtml.propTypes = {
    question: PropTypes.object,
    turkId: PropTypes.string,
    studyId: PropTypes.string,
    onDoneFn: PropTypes.func
}
