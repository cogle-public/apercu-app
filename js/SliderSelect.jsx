import React from "react";
import PropTypes from 'prop-types'
import _ from 'lodash'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'

import { Button, Grid, Row, Col } from "react-bootstrap";

export default class SliderSelect extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}, value: -1};
        this.valueSet = false;
        this.min = 0;
        this.max =100;
    }

    handleChangeStart() {}
    handleChangeComplete() {}

    handleOnChange(value) {
        this.setState({value: value});
        this.props.question.validValue = (value <= this.max && value >= this.min);
        this.props.question.answer = value;
        if (this.props.onDoneFn) {
            this.props.onDoneFn(value);
        }
    }

    render () {
        let question = this.props.question;
        let myThis = this;
        let value = this.state.value;
        let metaData = JSON.parse(question.meta_data);

        this.min=metaData.min || 0;
        this.max=metaData.max || 100;

        if (!this.valueSet) {
            this.valueSet = true;
            if (false && metaData.startValue) {
                value = metaData.startValue;
                question.answer = value;
                this.state.value = value;
                question.validValue = value <= this.max && value >= this.min;
            }
        }

        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>
                <div className='question-body  question-width'>
                    <div className='question-item slider-hbox'>
                        <div className='question-slider'>
                            <Slider
                                disabled={false}
                                min={this.min}
                                max={this.max}
                                value={value}
                                labels={metaData.labels ? metaData.labels : {}}
                                onChange={(v) => {this.handleOnChange(v)}}
                                onChangeStart={() => {this.handleChangeStart()}}
                                onChangeComplete={() => {this.handleChangeComplete()}}
                                tooltip={true}
                            />
                        </div>
                    </div>
                </div>
                {metaData.showValue
                    ?
                        <div  className='slider-hbox' >{value}
                            {metaData.valueChar || ''}
                        </div>
                    :<span></span>
                }
                {question.required
                        ?   
                        <div className='hbox' style={{marginTop: '20px'}}>
                            <br/>
                            {'*required'}
                        </div>
                        :
                        ''
                }
            </div>
        );
    }
}


SliderSelect.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
