import React from "react";
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class TextInput extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}, copied: false};
  }

  componentDidMount() {
    if (!this.props.question.answer) {this.props.question.answer = '';}
  }

  keyPress(e) {
        let answer = e.target.value;
        this.props.question.validValue = answer && answer.length > 0;
        this.props.question.answer = answer;
        if (this.props.onDoneFn) {
            this.props.onDoneFn(e.target.value);
        }
    }

    render () {
        let question = this.props.question;
        this.required = question.required;
        let metaData = {label:'', helperText: ''};
        if (question.meta_data) {metaData = JSON.parse(question.meta_data)}
        let disabled = metaData.disabled;
        if (metaData.completionCode) {
            question.answer = metaData.completionCode;
            this.state.completionCode = metaData.completionCode;
            if (!question.validValue) {
                question.validValue = true;
                setTimeout(() => {this.props.onDoneFn();}, 500);
            }
        }
        //console.log("Metadata is " + JSON.stringify(metaData));
        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>
                {metaData.completionCode
                    ?
                    <div className='question-vbox-clipboard'>
                       <div className='question-hbox'>
                       <div className='question-hbox'>
                       <TextField 
                            type="text" 
                            label={metaData.label}
                            onChange={(e) => {this.keyPress(e)}} 
                            variant="outlined"
                            value={metaData.completionCode} 
                            fullWidth={true}
                            helperText={metaData.helperText}
                        />
                        </div>
                        <CopyToClipboard 
                            text={this.state.completionCode}
                            onCopy={() => this.setState({copied: true})}>
                            <button><img width='64' height='auto'src="/static/images/copy-img.png"/></button>
                        </CopyToClipboard>
                           &nbsp;&nbsp;
                        <label className='align-self-center'>
                            Copy to clipboard
                        </label>
                    </div>
                    {this.state.copied ? <span style={{color: 'red'}}>&nbsp;Copied: '{this.state.completionCode}' into clipboard.</span> : null}
                    </div>
                    :
                    <div className={'question-body no-margin'} >
                    <TextField 
                        type="text" 
                        label={metaData.label}
                        onChange={(e) => {this.keyPress(e)}} 
                        variant="outlined"
                        required={question.required}
                        disabled={disabled}
                        defaultValue={question.answer} 
                        helperText={metaData.helperText}
                    />
                    </div>
                }    
                {question.required && !metaData.completionCode
                        ?   
                        <div className='hbox'>
                            {'*required'}
                        </div>
                        :
                        ''
                }
            </div>
        );
    }
}


TextInput.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
