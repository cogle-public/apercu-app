import React from "react";
import PropTypes from 'prop-types'
import _ from 'lodash'
const uuid = require('uuid/v4');

require('../css/questions.css');

import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

var $ = require('jquery');

export default class MultipleSelect extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}};
        this.uuid = uuid();
    }

    invertAnswer(answer) {
        let ans = this.state.answers;
        ans[answer] = !ans[answer];
        setTimeout(() => {this.setState({answers: ans});}, 300);
    }

    setSelection(e, answer, ignore) {
        e.preventDefault();
        if (ignore) {
            return;
        }
        this.invertAnswer(answer);
        let ans = [];
        _.each(Object.keys(this.state.answers), (k) => {if (this.state.answers[k]) {ans.push(k)}});
        this.props.question.validValue = (ans && ans.length > 0);
        this.props.question.answer = ans;
        if (this.props.onDoneFn) this.props.onDoneFn(answer + (_.includes(ans, answer) ? ' +' : ' -'));
    }

    render () {
        let question = this.props.question;
        let myThis = this;
        this.choices = JSON.parse(question.choices);
        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>



                <div className='question-body'>
                    <FormLabel component="legend">{(question.required ? 'pick one or more*' : 'pick 0 or more')}</FormLabel>
                    {_.map(this.choices, (choice, inx) => {
                        return (
                            <FormControlLabel
                                key={'ms_' + inx + '_' + myThis.uuid}
                                control={
                                    <Checkbox 
                                        checked={myThis.state.answers[choice]} 
                                        color="primary"
                                        onChange={(e) => {myThis.setSelection(e, choice, false)} }
                                    />
                                }
                                label={choice}
                            />
                        );
                    })
                    }
                </div>

                {question.required
                        ?   
                        <div className='hbox'>
                            <br/>
                            {'*required'}
                        </div>
                        :
                        ''
                }
            </div>
        );
    }
}


MultipleSelect.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
