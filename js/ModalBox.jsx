/***
 *  React Component to show modal boxes

 -------------------------------------------------------------------------------
 "Copyright (C) 2017, PARC, a Xerox company"
 -------------------------------------------------------------------------------

 */

/**
 * Created by krivacic on 4/3/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import "./ModalBox.css"

export default class ModalBox extends React.Component {
    constructor() {
        super();
        this.state = {};
    }

    render() {

        function exitCheck(myThis) {
            if (myThis.props.noExitCheck || myThis.props.tooltip) {
                return (<span></span>);
            }
            return (
                <div className="modal-exit-check">
                <img  src="exit.png" onClick={myThis.props.closeFn} alt="Exit">
                </img>
                </div>
            );
        }

        function getModalHeader(myThis) {
            if (myThis.props.title && myThis.props.title.length>0) {
                return (
                <div className="modal-header">
                    <span className="modal-title">{myThis.props.title}</span>
                    {exitCheck(myThis)}
                </div>);
            }
            return exitCheck(myThis);
        }

        function getModalFooter(footer) {
            if (footer) {
                return footer;
            }
            return <span></span>
        }

        if (this.props.showModal) {
            return (
                    <div className="modal-content" >
                        {getModalHeader(this)}
                        <div className="modal-body">
                            {this.props.modalComponent}
                        </div>
                        {getModalFooter(this.props.modalFooter)}
                    </div>
            );
        }
        return <span></span>
    }
};

ModalBox.propTypes = {
    showModal: PropTypes.bool,
    modalComponent: PropTypes.object,
    modalFooter: PropTypes.object,
    title: PropTypes.string,
};
