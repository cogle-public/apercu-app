/***
 *  PG_Helpers
 *  Helpers to talk to the server api.


-------------------------------------------------------------------------------
    "Copyright (C) 2018, PARC, a Xerox company"
-------------------------------------------------------------------------------

*/
import _ from 'lodash';
var $ = require('jquery');

let exports = {};

exports.geStudyUser = function(turkId, studyId, resultFn) {
    //console.log("$GET in action");
    var is_touch_screen = encodeURI(('ontouchstart' in document.documentElement).toString());
    var browser = window.navigator.userAgent;

    var browser_info = encodeURIComponent(JSON.stringify({
        browser: browser,
        is_touch_screen: is_touch_screen,
        window_size: {width: window.innerWidth, height: window.innerHeight}
    }));

    $.get( encodeURI("/api/get_study_user/?turk_id="+turkId+"&study_id="+studyId+"&browser_info="+browser_info),
        function(data) {
            //console.log("Get study user OK ");
            //console.log(data);
            resultFn(data);
        },
        "json");
}

exports.geStudyUserAppDict = function(turkId, studyId, resultFn) {
    $.get( encodeURI("/api/get_study_user_app_dict/?turk_id="+turkId+"&study_id="+studyId),
        function(data) {
            resultFn(data);
        },
        "json");
}

exports.reportUserAnswers  = function(turkId, studyId, instrumentSequenceId, answers, resultFn) {
    //console.log("post answers: " + JSON.stringify(answers));
    $.post( "/api/report_user_answers/?turk_id="+turkId+"&study_id="+studyId+"&instrument_sequence_id="+instrumentSequenceId+(loggingOn?"":"&nodb=true"), JSON.stringify(answers),
        function(data){
            //console.log("Post result");
            //console.log(data);
            resultFn(data);
        },
        "json");
}

exports.fetchStaticFile = function(turkId, studyId, study_dir, staticFileName, resultFn) {
    //console.log("Fetching file " + staticFileName);
    $.get("/api/get_static_file/?turk_id="+turkId+"&study_id="+studyId+"&study_dir="+study_dir+"&study_fn="+staticFileName,
        function(data) {
            //console.log("Get static file ");
            resultFn(data);
        });
}

let loggingOn = true;

exports.setLoggingState = function(logging) {
    loggingOn =  logging;
}

exports.log  = function(turkId, studyId, elementId, logStr) {
    if (loggingOn) {
        //console.log("log " + turkId + "&study_id=" + studyId + "&element_id=" + elementId, JSON.stringify({dataStr: (logStr || '')}));
        $.post("/api/log/?turk_id=" + turkId + "&study_id=" + studyId + "&element_id=" + elementId, JSON.stringify({dataStr: (logStr || '')}), _.noop);
    }
}

exports.getEnvironInfo = function(resultFn) {
    $.get("/api/get_environ_info/",
        function(data) {
            //console.log("Get environ " + JSON.stringify(data));
            resultFn(data);
        });
}
export default exports;
