import React from "react";
import PropTypes from 'prop-types'
import _ from 'lodash'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'

import { Button, Grid, Row, Col } from "react-bootstrap";

export default class TLX extends React.Component {
    constructor() {
        super();
        this.state = {answers: {}, value: -1};
        this.valueSet = false;
        this.min = 0;
        this.max =100;
        this.canvasRef = undefined;
        this.value = 0;
        this.widgetXMargin = 0;
        this.widgetYBarHeight = 20;
        this.pixPer5 = 5;
        this.pixPerInc = 5;     //pixels per numeric increment
    }

    canvasClick(e) {
        let bounds = this.canvasRef.getBoundingClientRect();
        let posx = Math.min(100,Math.max(0,Math.floor((e.clientX - bounds.left + (this.pixPerInc/2))  / this.pixPerInc)));
        //console.log("Canvas click " + posx);
        this.value = posx;
        this.valueSet = true;
        this.forceUpdate();
        this.props.question.validValue = true;
        this.props.question.answer = this.value;
        this.props.onDoneFn(this.value);
    }

    paintCanvas() {
        if (this.canvasRef && this.canvasDiv) {
            let barWidth = 2;
            let bounds = this.canvasRef.getBoundingClientRect();
            let width = bounds.width;
            let height = bounds.height;
            this.widgetYBarHeight = height/2 - 2;
            this.canvasRef.width = width;
            this.canvasRef.height = height;
            //console.log("Canvas Div is " + width + " x " + height);
            let ctx = this.canvasRef.getContext("2d");

            ctx.save();
            ctx.strokeStyle = '#000000';
            ctx.lineWidth = barWidth;
            ctx.beginPath();
            ctx.clearRect(0, 0, width, height);
            ctx.moveTo(this.widgetXMargin, height-barWidth);
            ctx.lineTo(width-this.widgetXMargin, height-barWidth);
            ctx.closePath();
            ctx.stroke();
            this.pixPer5 = (width-(this.widgetXMargin*2)-barWidth) / 20;
            this.pixPerInc = (width-(this.widgetXMargin*2)-barWidth) / 100;

            for (var i=0; i<=20; i++) {
                let w = this.widgetXMargin + (i*this.pixPer5);
                let h = height-this.widgetYBarHeight;
                if (i === 10) {
                    h = h - this.widgetYBarHeight;
                }
                ctx.beginPath();
                ctx.moveTo(w, height-barWidth);
                ctx.lineTo(w, h);
                ctx.closePath();
                ctx.stroke();
            }

            if (this.valueSet) {
                let w = Math.floor(this.widgetXMargin + ((this.value/5)*this.pixPer5)) ;
                //ctx.lineWidth = barWidth*2;
                ctx.lineWidth = Math.floor(this.pixPerInc);
                ctx.beginPath();
                ctx.moveTo(w, height-barWidth);
                ctx.lineTo(w, height-this.widgetYBarHeight*2);
                ctx.closePath();
                ctx.strokeStyle = '#0777E8';
                ctx.stroke();
            }
            ctx.restore();
        }
    }

    render () {
        let question = this.props.question;
        let myThis = this;
        let value = this.state.value;
        let metaData = JSON.parse(question.meta_data);
        this.min=metaData.min || 0;
        this.max=metaData.max || 100;

        setTimeout(() => {this.paintCanvas()}, 100);

        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>
                <div className='question-body  question-width'>
                    <div className='question-item vbox'>
                        <div className='question-tlx'
                             ref={item => this.canvasDiv = item}
                        >
                            <canvas
                                onClick={(e)=>this.canvasClick(e)}
                                ref={item => this.canvasRef = item}
                            >
                            </canvas>
                        </div>
                        <div className={'hbox hbox-between'} >
                            <span>Very Low</span>
                            <span>Very High</span>
                        </div>
                    </div>
                </div>
                {question.required
                    ?
                    <div className='hbox'>
                        <br/>
                        {'*required'}
                    </div>
                    :
                    ''
                }
            </div>
        );
    }
}


TLX.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
