import React from "react";
import PropTypes from 'prop-types'
import MultipleChoice from "./MultipleChoice";
import MultipleSelect from "./MultipleSelect";
import TextQuestion from "./TextInput"
import SliderSelect from "./SliderSelect";
import TextArea from "./TextArea"
import TLX from "./TLX";
import Likert from "./Likert"
import FileHtml from "./FileHtml"
import Parser from 'html-react-parser';
import TextField from '@material-ui/core/TextField';
import PGHelpers from "./PGHelpers";
import _ from 'lodash';
import Button from "@material-ui/core/Button";

require('../css/questions.css');

export default class QuestionPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {'fileHtml': ''};
        this.popups_opened = {};
        this.needDictCheck = false;
        this.appDictCheckB = (appDict) => {this.appDictCheck(appDict)};
        this.timeoutCheckB = () => {this.timeoutCheck()};
        this.waitingTimeout = false;
    }

    //
    // Check if an external app has completed
    //
    appDictCheck(appDictStr) {
        if (appDictStr && appDictStr.length > 0) {
            let appDict = JSON.parse(appDictStr);
            _.each(this.props.questions.elements, (q) => {
                if (q.needDictCheck) {
                    let adv = appDict[q.name + '.done'];
                    q.validValue = q.validValue || adv;
                    if (adv) {this.questionDone(null, null)}
                }
            })
        }
    }

    waitTimeout() {
        if (!this.waitingTimeout) {
            this.waitingTimeout = true;
            setTimeout(this.timeoutCheckB, 1000);
        }
    }

    timeoutCheck() {
        this.waitingTimeout = false;
        if (this.needDictCheck) {
            this.waitTimeout();
            PGHelpers.geStudyUserAppDict(this.props.turkId, this.props.studyId, this.appDictCheckB)
        }
    }

    componentDidMount() {
        this.questionDone(undefined, '');
        this.mountTime = _.now();
    }

    goNextPage(e, question, logStr) {
        if (question) {PGHelpers.log(this.props.turkId, this.props.studyId, question.uuid, logStr);}
        this.props.goNextPage(e);
    }

    questionDone(question, logStr) {
        let ready = false;
        let myThis = this;

        //console.log("Question done " + JSON.stringify(question) + ', ' + logStr);

        if (question) {PGHelpers.log(this.props.turkId, this.props.studyId, question.uuid, logStr);}
        let allAnswered = true;
        // check to see if all answered
        _.each(this.props.questions.elements, (q) => {
            allAnswered = allAnswered && (q.validValue || !q.required);
            if (q.meta_data) {
                let meta_data = JSON.parse(q.meta_data)
                if (meta_data.hang_time) {
                    let hangDone = (_.now() > (this.mountTime + (meta_data.hang_time * 1000)))
                    allAnswered = allAnswered && hangDone;
                    if (!hangDone) {
                        setTimeout(() => {myThis.questionDone(undefined, '');}, 500);
                    }
                }
            }
            //console.log("Post Check " + JSON.stringify(q.type) + " all: " + allAnswered);
       });
       this.needDictCheck = false;
       this.props.onDoneFn(allAnswered);
    }

    popupCheck(url) {
        if (!(url in this.popups_opened) && this.popup) {
            this.popups_opened[url] = true;
            this.popup.click();
            //window.open(url, 'External Window');
        }
    }

    makeUrl(question) {
        //console.log("Making a url from: ");
        //console.log(question);
        let metaData = JSON.parse(question.meta_data);
        let question_name = question.name;
        let url = '';
        this.rightSidePct = 50;
        if (metaData.url) {
            let meta_url = metaData.url;
            if (metaData.rightSidePct) {this.rightSidePct = metaData.rightSidePct}
            var replacements = meta_url.split('?');
            url = replacements[0] + "?study_id=" + this.props.studyId;
            var turkId = this.props.turkId;
            if (turkId && turkId.length > 0) {
                url = url + '&user_id=' + turkId;
            }
            if (replacements.length > 1) {
                //console.log("Looking to split args " + replacements[1]);
                _.each(replacements[1].split('&'), (item) => {
                    url = url + '&' + item;
                });
            }
            if (this.props.logHost && this.props.logHost.length > 0) {
                url = url + "&log_host=" + this.props.logHost;
            }
            //console.log("Question name: " + question_name);
            if (question_name) {
                url = url + "&question_name=" + question_name;
            }
        } else if (metaData.fileName) {
            url= "/api/get_static_file/?turk_id="+this.props.turkId+"&study_id="+this.props.studyId+"&study_dir="+metaData.study_dir+"&study_fn="+metaData.fileName;
        }
        //console.log("Fetch URL: " + url);
        return url;
    }

    render () {
        let myThis = this;
        myThis.needDictCheck = false;
        //console.log("RENDER questions:");
        //console.log(this.props.questions);
        //console.log("Show questions, side window: " + this.props.parent.state.sideWindowEnabled);
        let r =
                <div className='page-contents'>

                {this.props.questions.title && this.props.questions.title.length > 1
                    ?
                    <div className='page-header' >
                        {this.props.questions.title}
                    </div>
                    : <span></span>
                }

                <div className={'hbox' + (myThis.props.parent.state.sideWindowEnabled ? ' side-window-height' : '')} >
                <div className='page-elements' style={{width: (myThis.props.parent.state.sideWindowEnabled ? (100 - this.rightSidePct) + '%' : '100%')}}>
                {_.map(this.props.questions.elements, (question, inx) => {
                    let qp = this.props.pageNum + '.' + inx + ": ";
                    qp = '';
                    switch(question.type) {
                        case 'text_area':
                            return (
                                <TextArea key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                        case 'text':
                            return (
                                <TextQuestion key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                            break;
                        case 'likert':
                            return (
                                <Likert key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                            break;
                        case 'slider':
                            return (
                            <SliderSelect key={'q_'+inx}
                                question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                        case 'multiple_choice':
                            return (
                                <MultipleChoice  key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                        case 'multiple_select':
                            return (
                                <MultipleSelect  key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                        case 'tlx':
                            return (
                                <TLX  key={'q_'+inx}
                                                 question={question}
                                                 onDoneFn={(ans) => myThis.questionDone(question, ans)}
                                />
                            );
                        case 'html':
                            myThis.questionDone(question, 'view-html');
                            return Parser(question.text);
                        case 'file-html':
                            return (
                                <FileHtml  key={'q_'+inx}
                                    question={question}
                                    questPre={qp}
                                    turkId={myThis.props.turkId}
                                    studyId={myThis.props.studyId}
                                    onDoneFn={(ans) => myThis.questionDone(question, 'view-file')}
                                />
                                );
                        case 'pop_up':
                            var meta_data =  JSON.parse(question.meta_data);
                            var url = meta_data.url ? myThis.makeUrl(question) : '';
                            setTimeout(function() {myThis.popupCheck(url)}, 500);
                            var target = meta_data.tab_name || 'Popup';
                            return (
                                <div>
                                    <a href={url} target={target} style={{display: 'none'}}
                                       ref={item => myThis.popup = item}
                                    >
                                    </a>
                                </div>);
                            myThis.needDictCheck = question.required || myThis.needDictCheck;
                            question.needDictCheck = question.required;
                        case 'iframe_overlay':
                            var meta_data =  JSON.parse(question.meta_data);
                            var url = myThis.makeUrl(question);
                            myThis.needDictCheck = question.required || myThis.needDictCheck;
                            question.needDictCheck = question.required;
                            return (
                                <div className={'iframe_overlay'}>
                                    <iframe src={url} style={{border:'none', height: '100%', width:'100%'}}>MY IFRAME to {url}</iframe>
                                    <div className={'continue-floater'} >
                                        <Button className='right'
                                                disabled={false} onClick={(e) => {myThis.goNextPage(e, question, 'iframe_overlay')}}
                                                variant="contained"
                                                size="large"
                                        >
                                            Continue
                                        </Button>
                                    </div>
                                </div>
                            );
                        case 'iframe_inner':
                            var meta_data =  JSON.parse(question.meta_data);
                            var url = myThis.makeUrl(question);
                            myThis.needDictCheck = question.required || myThis.needDictCheck;
                            question.needDictCheck = question.required;
                            return (
                                <div className={'iframe_inner'}>
                                    <iframe src={url} frameborder="0" style={{overflow:'hidden',width:'100%'}}  >MY IFRAME to {url}</iframe>
                                </div>
                            );
                        case 'side_window':
                            var meta_data =  JSON.parse(question.meta_data);
                            var url = myThis.makeUrl(question);
                            myThis.props.parent.state.sideWindowEnabled = true;
                            myThis.props.parent.state.sideWindowUrl = url;
                            myThis.needDictCheck = question.required || myThis.needDictCheck;
                            question.needDictCheck = question.required;
                            return (<span></span>);
                        case 'para':
                            return (
                                <div className='question'  key={'q_'+inx} >
                                    <div className='question-head'>
                                        {question.text}&nbsp;
                                    </div>

                                </div>
                                )
                    }
                })}

                </div>
                    {myThis.props.parent.state.sideWindowEnabled
                        ?
                        <div className='question-side-window' style={{width: myThis.rightSidePct+'%', height: '100%'}}>
                            <iframe src={myThis.props.parent.state.sideWindowUrl} style={{border:'none', height: '90vh', width:'100%'}}></iframe>
                        </div>
                        : <span></span>
                    }
                </div>
                </div>
        ;
        if (this.needDictCheck) {
            this.waitTimeout();
        }
        return r;
    }
}

QuestionPage.propTypes = {
    parent: PropTypes.object,
    questions: PropTypes.object,
    turkId: PropTypes.string,
    studyId: PropTypes.string,
    logHost: PropTypes.string,
    onDoneFn: PropTypes.func
}
