/***
 *  React Component to show modal boxes

 -------------------------------------------------------------------------------
 "Copyright (C) 2017, PARC, a Xerox company"
 -------------------------------------------------------------------------------

 */

/**
 * Created by krivacic on 4/3/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button';
import "./ModalBox.css"

export default class ErrorPopup extends React.Component {
    constructor() {
        super();
        this.state = {};
    }

    itemChecked(itemName) {
        this.props.onChangeFn(itemName);
    }

    render() {
        function exitCheck(myThis) {
            if (myThis.props.noExitCheck) {
                return (<span></span>);
            }
            return (
                <div className="modal-exit-check">
                    <Button 
                        onClick={myThis.props.closeFn} 
                        variant="contained"
                    >
                        <img width='24px' height='auto' src={"static/exit.png"}  alt="Exit" />
                    </Button>
                </div>
            );
        }

        function getModalHeader(myThis) {
            if (myThis.props.title && myThis.props.title.length>0) {
                return (
                <div className="modal-header">
                    <span className="modal-title">{myThis.props.title}</span>
                    {exitCheck(myThis)}
                </div>);
            }
            return exitCheck(myThis);
        }

        function getModalFooter(footer) {
            if (footer) {
                return footer;
            }
            return <span></span>
        }

        const style = {position: 'absolute', top: '30%', left: '30%', right: '30%', bottom: '30%'};

        return (
            <div className='modal-popup-screen-background' >
                <div style={style} className={"modal-popup-content modal-error"} >
                    {getModalHeader(this)}
                    <div className="modal-body">
                        {this.props.modalContent}
                    </div>
                    {getModalFooter(this.props.modalFooter)}
                </div>
            </div>
       );

    }
};

ErrorPopup.propTypes = {
    noExitCheck: PropTypes.bool,
    modalContent: PropTypes.object,
    modalFooter: PropTypes.object,
    title: PropTypes.string,
    closeFn: PropTypes.func
};
