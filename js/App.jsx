import React from "react";
import QuestionPages from "./QuestionPages";
import RegistrationPage from "./RegistrationPage";
import PGHelpers from "./PGHelpers";
import ErrorPopup from "./ErrorPopup";
import QuestionPage from "./QuestionPage";

require('../css/questions.css');
require('../css/fullstack.css');

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.surveyOver = {
            title: 'Survey Over',
            elements: [
                {text: 'Thank you for participating, the survey is over.', type:'text'}
            ]
        };

        this.questionPages = {
            surveyTitle: 'Your survey is over',
            pages:[this.surveyOver]
        }


        var newWin = window.open('http://google.com');
        var popupsBlocked = false;
        if (!newWin || newWin.closed || typeof newWin.closed=='undefined') {
            //POPUP BLOCKED
            popupsBlocked = true;
        } else {
            newWin.close();
        }

        this.state = {
            regInfo: {turk_id: "", study_id: ""},
            questionPages: this.questionPages,
            needRegInfo: true,
            stateInfo: {display_state: 'registration'},
            minWidth: 0,
            minHeight: 0,
            autoloadStudy: '',
            popupsBlocked: popupsBlocked,
            logging: window.logging != 'off',
            resetLocal: window.reset_local
        }

        PGHelpers.setLoggingState(this.state.logging);
        if (this.state.resetLocal == 'true') {this.clearStorage();}

        let regInfoStr = localStorage.getItem('study.parc.com.registration', '{"turk_id": "", "study_id": ""}');
        this.state.regInfo = JSON.parse(regInfoStr);

        let stateStr = localStorage.getItem('study.parc.com.state', '{"display_state": "registration"}');
        if (stateStr) {
            this.state.stateInfo = JSON.parse(stateStr);
        }

        this.studyInfoStr = localStorage.getItem('study.parc.com.studyInfo');
        this.answerStr = localStorage.getItem('study.parc.com.answers');

        PGHelpers.getEnvironInfo((env) => {
            this.setState({
                autoloadStudy: env.autoload_study,
                minWidth: env.browser_min_width,
                minHeight: env.browser_min_height,
                host: env.host
            });
        });

    }

    setDisplayState(display_state) {
        let stateInfo = this.state.stateInfo;
        stateInfo.display_state = display_state;
        localStorage.setItem('study.parc.com.state', JSON.stringify(stateInfo));
        this.setState({stateInfo: stateInfo});
    }

    componentDidMount() {
        if (this.studyInfoStr) {
            this.setStudyInfo(JSON.parse(this.studyInfoStr), false)
        }
        if (this.answerStr) {
            this.state.answers = JSON.parse(this.answerStr);
        }
        switch (this.state.stateInfo.display_state) {
            case 'interview':
            case 'report_registration':
                if (this.state.regInfo) {
                    this.reportRegInfo(this.state.regInfo);
                }
                break;
            case 'reporting_answers':
                if (this.state.answers) {
                    this.reportAnswers(this.state.answers);
                }
                break;
            case 'completed':
                this.state.stateInfo.display_state = 'registration';
                break;
        }
    }

    clearStorage() {
        // keep the turk id for next time
        let regInfo = this.state.regInfo || {};
        regInfo.study_id = '';
        this.state.stateInfo.display_state = 'completed';
        localStorage.setItem('study.parc.com.studyInfo', JSON.stringify({}));
        localStorage.setItem('study.parc.com.registration', JSON.stringify(regInfo));
        localStorage.setItem('study.parc.com.answers', JSON.stringify({}));
    }

    reportOver() {
            this.setState({
                questionPages: this.questionPages, 
                studyInfo: {
                    uuid:'None',
                    name: 'Done'
                }
            });
            this.clearStorage();
            this.setDisplayState('completed');
    }

    setStudyInfo(data, writeIt) {
        if (data) {
            //console.log("Got back data " + data.result);
            if (data.result == 'OK') {
                if (data.pageInfo && data.pageInfo.progress && this.state.studyInfo && this.state.studyInfo.pageInfo && this.state.studyInfo.pageInfo.progress) {
                    data.pageInfo.progress = Math.max(data.pageInfo.progress, this.state.studyInfo.pageInfo.progress)
                }
                if (writeIt) {
                    if (data.completed) {
                        this.completionCode = data.completion_code;
                        this.reportOver();
                    } else {
                        this.setState({questionPages: data.pageInfo, studyInfo: data});
                        this.setDisplayState('interview');
                        localStorage.setItem('study.parc.com.studyInfo', JSON.stringify(data));
                    }
                } else {
                    this.state.questionPages = data.pageInfo;
                    this.state.studyInfo = data;
                }

            } else if (data.result == 'bad_study_id') {
                this.setState({errorMsg: 'Bad study id', showError: true, errorTitle: 'Error in registration'});
                this.setDisplayState('registration');
            } else if (data.result == 'not_now') {
                this.setDisplayState('not_now');
            } else {
                this.reportOver();
            }
        }
    }

    reportRegInfo(regInfo) {
        let needRegInfo = regInfo.turk_id.length < 1 || regInfo.study_id.length < 1;
        this.setState({regInfo: regInfo});
        if (!needRegInfo) {
            this.setDisplayState('report_registration');
            PGHelpers.geStudyUser(regInfo.turk_id, regInfo.study_id, (data) => {
                this.setStudyInfo(data, true);
            });
        }
    }

    reportAnswers(answers) {
        let myThis = this;

        if (this.state.regInfo && this.state.studyInfo) {
            localStorage.setItem('study.parc.com.answers', JSON.stringify(answers));
            this.setDisplayState('reporting_answers');

            var answerDict = {};
            // build the answer dictionary
            _.each(answers.pages, (page) => {
                _.each(page.elements, (element) => {
                    if (element.hasOwnProperty('name') && element.name.length > 0) {
                        answerDict[element.name] = element.answer;
                    }
                });
            });

            //console.log("Reported answer dict: " + JSON.stringify(answerDict));

            answers.answer_dict = answerDict;

            PGHelpers.reportUserAnswers(this.state.regInfo.turk_id, this.state.regInfo.study_id, this.state.studyInfo.current_instrument_sequence, answers, (data) => {
                myThis.setStudyInfo(data, true);
            });
        } else {
            this.completionCode = 'unknown';
            this.clearStorage();
        }
    }

    questionsDone(done) {
        //console.log("Questions ALL done: " + done)
    }

    closeModal() {
        this.setState({showError: false});
    }

    render () {

        function getMainDisplay(myThis) {
            switch (myThis.state.stateInfo.display_state) {
                case 'completed':
                    return (
                        <div className='survey-contents'>
                            <div className='survey-header'>
                                <h2>Thank you</h2>
                            </div>
                            <div className='page-contents question-vbox'>
                                <div className='page-header' >
                                    Your task is completed.
                                </div>
                                <br/>
                                <div className='completed-info' >
                                    Your last completion code was: '{myThis.completionCode}'
                                </div>
                            </div>
                        </div>
                        );
                case 'not_now':
                    return (
                        <div className='survey-contents'>
                            <div className='survey-header'>
                                <h2>Thank you</h2>
                            </div>
                            <div className='page-contents question-vbox'>
                                <div className='page-header' >
                                    There are no tasks for you at the present time.
                                </div>
                            </div>
                        </div>
                    );
                case 'registration':
                    return (
                        <RegistrationPage 
                            regInfo={myThis.state.regInfo}
                            autoloadStudy={myThis.state.autoloadStudy}
                            onDoneFn={(reg) => myThis.reportRegInfo(reg)}
                        />
                        );
                case 'report_registration':
                    return (
                        <div>
                            <h2>Reporting Registration</h2>
                        </div>
                        ); 
                case 'reporting_answers':
                    return (
                        <div>
                            <h2>Reporting Answers</h2>
                        </div>
                        ); 
                case 'interview':
                    return (
                        <QuestionPages
                            parent={myThis}
                            turkId={myThis.state.regInfo.turk_id}
                            studyId={myThis.state.regInfo.study_id}
                            studyInfo={myThis.state.studyInfo}
                            logHost={myThis.state.host}
                            questionPages={myThis.state.questionPages}
                            reportAnswers={(data) => myThis.reportAnswers(data)}
                            onDoneFn={(done) => myThis.questionsDone(done)}
                        />
                        );
            }
        }

        let iw = window.innerWidth;
        let ih = window.innerHeight;
        if (iw < this.state.minWidth || ih < this.state.minHeight) {
            this.state.showError = true;
            this.state.errorTitle = "Browser size error";
            this.state.errorMsg =
                <div className='survey-contents'>
                    <div className='survey-header'>
                        <h2>Sorry</h2>
                    </div>
                    <div className='page-contents vbox'>
                        <div className='error-header' >
                            Your browser size of {iw} x {ih}
                            &nbsp;
                            does not meet minimum requirements of:
                            &nbsp;
                            {this.state.minWidth} x {this.state.minHeight}
                        </div>
                        <div className='error-note'>
                            Hint:  Try your browser in full-screen mode, or
                            decrease your font size.
                        </div>
                    </div>
                </div>
        }
        
        if (this.state.popupsBlocked && this.state.studyInfo &&  this.state.studyInfo.study_info && this.state.studyInfo.study_info.popups_required) {
            this.state.showError = true;
            this.state.errorTitle = "Popup blocker detected";
            this.state.errorMsg =
                <div className='survey-contents'>
                    <div className='survey-header'>
                        <h2>Sorry</h2>
                    </div>
                    <div className='page-contents vbox'>
                        <div className='error-header' >
                            Sorry, you need to turn off your browsers popup blocker to participate in this study.
                        </div>
                        <div className='error-note'>
                            Hint: Open your browser settings and turn off the popup blocker, then reload this page.
                        </div>
                    </div>
                </div>
        }

        return (
                <div className=''>
                    {getMainDisplay(this)} 
                    {this.state.showError
                        ? <ErrorPopup 
                            modalContent={this.state.errorMsg}
                            title={this.state.errorTitle}
                            closeFn={() => {this.closeModal()}}   
                          />
                        : <span></span>
                    }
                </div>
        );
    }
}
