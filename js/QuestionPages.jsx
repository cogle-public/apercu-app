import React from "react";
import PropTypes from 'prop-types'
import QuestionPage from "./QuestionPage"
import Button from '@material-ui/core/Button';
import PGHelpers from "./PGHelpers";
import '@material/react-linear-progress/dist/linear-progress.css';
import LinearProgress from '@material/react-linear-progress';
import TextField from "@material-ui/core/TextField";

require('../css/questions.css');

export default class QuestionPages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageInx: 0,
            nextEnabled: false,
            sideWindowEnabled: false,
            destScore: 0,
            score: 0,
            timerRunning: false,
            workState: false
        };
    }

    nextPage(done, qp) {
        this.qp = qp;
        this.setState({nextEnabled: done});
    }

    goNextPage(e) {
        e.preventDefault();
        PGHelpers.log(this.props.turkId, this.props.studyId, 'Continue', 'Continue');
        this.state.pageInx = this.state.pageInx + 1;
        if (this.state.pageInx >= this.props.questionPages.pages.length) {
            this.props.reportAnswers(this.props.questionPages);
            this.setState({pageInx: 0, nextEnabled: false, sideWindowEnabled: false});
        } else {
            this.setState({pageInx: this.state.pageInx, nextEnabled: false, sideWindowEnabled: false});
        }
    }

    scoreTimer() {
        if (this.state.destScore > this.state.score) {
            var newScore = this.state.score + 1;
            this.state.score = newScore;
            this.state.timerRunning = true;
            setTimeout(()=>this.scoreTimer(), 1000);
            this.forceUpdate();
        } else {
            this.state.timerRunning = false;
        }
    }

    render () {
        let myThis = this;
        let progressPos = 0;
        let myStyle = {}

        if (this.props.studyInfo &&  this.props.studyInfo.pageInfo && this.props.studyInfo.pageInfo.progress) {
            progressPos = this.props.studyInfo.pageInfo.progress;
        }

        if (this.props.studyInfo && (this.state.destScore !== this.props.studyInfo.score)) {
            this.state.destScore = this.props.studyInfo.score;
            if (this.props.parent.lastReportedScore === this.props.studyInfo.score) {
                this.state.score = this.state.destScore;
            }
            this.props.parent.lastReportedScore = this.state.destScore;
            // Just set the score for now, no more Countdown of the score
            // comment out the next line to get the score countdown working
            this.state.score = this.state.destScore;
        }

        if (this.state.score < this.state.destScore && !this.state.timerRunning) {
            this.state.timerRunning = true;
            setTimeout(()=>this.scoreTimer(), 1000);
        }

        if ('meta_data' in this.props.questionPages && this.props.questionPages.meta_data && this.props.questionPages.meta_data.length() > 1) {
            var meta_data =  JSON.parse(this.props.questionPages.meta_data);
            if ('style' in meta_data) {
                myStyle = meta_data.style;
            }
        }

        return (
            <div className='survey-contents' style={myStyle} >
                <div className='survey-header' >
                    {this.props.questionPages.surveyTitle && this.props.questionPages.surveyTitle.length > 1 ? this.props.questionPages.surveyTitle : ''}
                </div>

                {_.map(this.props.questionPages.pages, (qp, inx) => {
                    if (inx == myThis.state.pageInx) {
                        //console.log("Showing Inx: " + inx);
                    }
                    return ( (inx == myThis.state.pageInx) 
                        ?
                            <QuestionPage key={'qp_' + inx}
                                parent={myThis}
                                questions={qp}
                                pageNum={inx+1}
                                turkId={myThis.props.turkId}
                                studyId={myThis.props.studyId}
                                logHost={myThis.props.logHost}
                                onDoneFn={(done) => myThis.nextPage(done, qp)}
                                goNextPage={(e) => myThis.goNextPage(e)}
                            />
                        :
                            <span key={'qp_' + inx}></span>
                    )
                })}

                <div className='footer'>
                    <div className='question-hbox'>
                        <div>
                                {'* required fields'}
                        </div>
                        {this.props.studyInfo &&  this.props.studyInfo.study_info && this.props.studyInfo.study_info.show_progress
                            ? <div className={'margin-auto'} style={{height: '30px', width: '300px'}} >
                                <LinearProgress
                                    buffer={1}
                                    progress={progressPos}
                                    bufferingDots={false}
                                />
                            </div>
                            : <span></span>
                        }
                        {this.props.studyInfo &&  this.props.studyInfo.study_info && this.props.studyInfo.study_info.show_score
                            ? <div className={'margin-auto'} style={{height: '30px', width: '300px'}} >
                                Score:&nbsp;&nbsp;
                                <input
                                    id={'score_input'}
                                    className={'score_input' + (myThis.state.destScore === myThis.state.score ? ' new_score_input_0' : ' new_score_input_diff') }
                                    type={'text'}
                                    value={myThis.state.score}
                                />
                            </div>
                            : <span></span>
                        }
                        <Button className='right'
                            disabled={!this.state.nextEnabled} onClick={(e) => {this.goNextPage(e)}}
                            variant="contained"
                            size="large"
                        >
                            Continue
                        </Button>
                    </div>
                </div> 
            </div>
        );
    }
}

QuestionPages.propTypes = {
    turkId: PropTypes.string,
    studyId: PropTypes.string,
    studyInfo: PropTypes.object,
    logHost: PropTypes.string,
    currentInstrumentSequenceId: PropTypes.string,
    questionPages: PropTypes.object,
    reportAnswers: PropTypes.func,
    onDoneFn: PropTypes.func
}
