import React from "react";
import PropTypes from 'prop-types'
import _ from 'lodash'
const uuid = require('uuid/v4');

import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';

require('../css/questions.css');

import {MDCFormField} from '@material/form-field';
import {MDCRadio} from '@material/radio';

var $ = require('jquery');

export default class MultipleChoice extends React.Component {
    constructor() {
        super();
        this.state = {lastAnswer: ''};
        this.uuid = uuid();
    }

    setSelection(e, answer) {
        e.preventDefault();
        let myThis = this;
        this.props.question.validValue = true
        this.props.question.answer = answer
        if (this.props.onDoneFn) {this.props.onDoneFn(answer);}
        setTimeout(() => {myThis.setState({lastAnswer: answer})}, 200);
   }

    render () {
        let question = this.props.question;
        this.choices = JSON.parse(question.choices);
        let myThis = this;
        return (
            <div className='question'>
                <div className='question-head'>
                    {question.text}&nbsp;
                </div>

                <div className='question-body'>
                    <FormLabel component="legend">
                        {'pick one' + (question.required ? '*' : '')}
                    </FormLabel>
                    {_.map(this.choices, (choice, inx) => {
                        return (
                            <FormControlLabel
                                key={'mc_' + inx + '_' + myThis.uuid}
                                control={
                                    <Radio 
                                        checked={myThis.state.lastAnswer == choice} 
                                        onChange={(e) => {myThis.setSelection(e, choice, false)} }
                                        color="primary"
                                        disableRipple={false}
                                    />
                                }
                                label={choice}
                            />
                        );
                    })
                    }
                </div>

                {question.required
                        ?   
                        <div className='hbox'>
                            <br/>
                            {'*required'}
                        </div>
                        :
                        ''
                }
            </div>
        );
    }
}


MultipleChoice.propTypes = {
    question: PropTypes.object,
    onDoneFn: PropTypes.func
}
