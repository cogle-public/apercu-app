/***
 *  React Component to show modal boxes

 -------------------------------------------------------------------------------
 "Copyright (C) 2017, PARC, a Xerox company"
 -------------------------------------------------------------------------------

 */

/**
 * Created by krivacic on 4/3/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import "./ModalBox.css"

export default class ModalPopup extends React.Component {
    constructor() {
        super();
        this.state = {};
    }

    itemChecked(itemName) {
        this.props.onChangeFn(itemName);
    }

    render() {
        function exitCheck(myThis) {
            if (myThis.props.noExitCheck) {
                return (<span></span>);
            }
            return (
                <div className="modal-exit-check">
                <img  src="exit.png" onClick={myThis.props.closeFn} alt="Exit">
                </img>
                </div>
            );
        }

        function getModalHeader(myThis) {
            if (myThis.props.title && myThis.props.title.length>0) {
                return (
                <div className="modal-header">
                    <span className="modal-title">{myThis.props.title}</span>
                    {exitCheck(myThis)}
                </div>);
            }
            return exitCheck(myThis);
        }

        function getModalFooter(footer) {
            if (footer) {
                return footer;
            }
            return <span></span>
        }

        const style = {position: 'absolute', top: '30%', left: '30%', right: '30%', bottom: '30%'};

        return (
                        <div style={style} className={"modal-popup-content " + (this.props.errorModal ? 'modal-error' : '')} >
                            {getModalHeader(this)}
                            <div className="modal-body">
                                {this.props.modalContent}
                            </div>
                            {getModalFooter(this.props.modalFooter)}
                        </div>
        );

    }
};

ModalPopup.propTypes = {
    noExitCheck: PropTypes.bool,
    modalContent: PropTypes.object,
    modalFooter: PropTypes.object,
    position: PropTypes.object,
    title: PropTypes.string,
    closeFn: PropTypes.func
};
