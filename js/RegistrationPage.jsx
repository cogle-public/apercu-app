import React from "react";
import PropTypes from 'prop-types'
import TextQuestion from "./TextInput"
import TextArea from "./TextArea"
import PGHelpers from "./PGHelpers";
import Button from '@material-ui/core/Button';
const uuid = require('uuid/v4');

require('../css/questions.css');


export default class RegistrationPage extends React.Component {
    constructor(props) {
        super(props);
        this.regInfo = this.props.regInfo || {turk_id: '', study_id: ''};
        if (this.regInfo.turk_id.length < 1) {
            this.regInfo.turk_id = uuid();
            localStorage.setItem('study.parc.com.registration', JSON.stringify(this.regInfo));
        }
        this.getTurkIdQ = {text: 'Please enter your Turk Id.',  meta_data: JSON.stringify({label: "Turk Id"}),  type:'text', required: true};
        this.getStudyIdQ = {text: 'Please enter the study Id.', meta_data: JSON.stringify({label: "Study Id"}), type:'text', required: true};
        this.getTurkIdQ.answer = this.regInfo.turk_id;
        this.getStudyIdQ.answer = this.regInfo.study_id;
        this.state = {
            allAnswered: false
        }
    }

    checkRegInfo() {
        this.getTurkIdQ.validValue = this.getTurkIdQ.answer && this.getTurkIdQ.answer.length > 0;
        this.getStudyIdQ.validValue = this.getStudyIdQ.answer && this.getStudyIdQ.answer.length > 0;        
    }

    setRegInfo() {
        this.regInfo.turk_id = this.getTurkIdQ.answer;
        this.regInfo.study_id = this.getStudyIdQ.answer;
        localStorage.setItem('study.parc.com.registration', JSON.stringify(this.regInfo));      
        this.checkRegInfo();
        this.setState({allAnswered: this.getTurkIdQ.validValue && this.getStudyIdQ.validValue});
    }

    questionDone(question, ans) {
        this.setRegInfo();
    }

    goNextPage(e) {
        PGHelpers.log(this.regInfo.turk_id || 'no-turk-id', this.regInfo.study_id || 'register','', 'Register');
        this.props.onDoneFn(this.regInfo);
    }

    render () {
        let myThis = this;
        this.checkRegInfo();
        this.state.allAnswered = this.getTurkIdQ.validValue && this.getStudyIdQ.validValue;

        if (this.props.autoloadStudy && this.props.autoloadStudy.length > 0) {
            this.regInfo.study_id = this.props.autoloadStudy;
            this.getStudyIdQ.answer = this.props.autoloadStudy;
            localStorage.setItem('study.parc.com.registration', JSON.stringify(this.regInfo));
            this.checkRegInfo();
            this.goNextPage();
        }

        //console.log('render reg page, autoload: ' + this.props.autoloadStudy + ', study: ' + this.regInfo.study_id);
        return (
            <div className='survey-contents'>
                <div className='survey-header' >
                    {'Task Registration'}
                </div>

                <div className='page-contents'>

                    <div className='page-header' >
                        Please enter your registration information
                    </div>

                    <div>
                        <br/>
                    </div>

                    <div className='hbox'>
                        <div className='page-elements' style={{width: '100%'}}>

                            <TextQuestion
                                question={this.getStudyIdQ}
                                onDoneFn={(ans) => this.questionDone(this.getStudyIdQ, ans)}
                            />

                            <div className={'question-head'}>
                                When you enter in the Study ID you were provided, please hit the green CONTINUE button at the bottom of the page.
                            </div>

                        </div>
                    </div>
                    <div className='footer'>
                        <div className='question-hbox'>
                            <span>
                                {'* required fields'}
                            </span>
                            <Button className='right'
                                disabled={!this.state.allAnswered} 
                                onClick={(e) => {this.goNextPage(e)}}
                                variant="contained"
                                size="large"
                                >
                                Continue
                            </Button>
                        </div>
                    </div> 

                </div>
            </div>
        );
    }
}

RegistrationPage.propTypes = {
    regInfo: PropTypes.object,
    onDoneFn: PropTypes.func
}
