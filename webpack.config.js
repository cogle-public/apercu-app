const webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
    entry:  __dirname + '/js/index.jsx',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
    },
    resolve: {
        extensions: [".js", ".jsx", ".css", '.sccs']
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                })
            },
            {
                test: /\.(scss)$/,            
                include: [__dirname  , '/node_modules/@material'],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                                 importLoaders: 1
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                 importLoaders: 1
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                importer: function(url, prev) {
                                    if(url.indexOf('@material') === 0) {
                                        var filePath = url.split('@material')[1];
                                        var nodeModulePath = './node_modules/@material/' + filePath;
                                        return { file: require('path').resolve(nodeModulePath) };
                                    }

                                    if(url.indexOf('material-components-web') === 0) {
                                        var nodeModulePath = './node_modules/material-components-web/material-components-web';
                                        return { file: require('path').resolve(nodeModulePath) };
                                    }

                                    return { file: url };
                                }
                            }
                        }
                    ],
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: 'file-loader'
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('styles.css'),
    ]
};

module.exports = config;
