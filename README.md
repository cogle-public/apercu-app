# apercu-app

This is the web front end for the apercu survey service.

### Dependencies

You will need to load up all of the apercu repositories

* apercu-build
* apercu-studies
* apercu-service
* apercu-app

Build on top of:

* Javascript
* React
* @Material-ui 
* etc

Uses Babel, npm, webpack, etc for building.

### Building

To build the 'build' directory to push over to the apercu-build project, use

``` npm run-script build ```

### Local Debugging

When running with a local service, the 'dist' build directory sym links to the apercu-build directory that the service will be looking to serve.

For local debugging use

``` npm run-script watch ```

Which will continuously watch for changes and build the bundle.js being served.


### Reminder

Use npm install when you first download the project, or whenever the package.json changes.
